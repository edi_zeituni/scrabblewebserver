/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ws;

import game.model.Board;
import game.model.Cell;
import ws.scrabble.*;
import game.model.Deck;
import game.model.Game;
import game.model.Player;
import game.model.PlayersManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;


/**
 *
 * @author danielo
 */
@WebService(serviceName = "ScrabbleWebServiceService", portName = "ScrabbleWebServicePort", endpointInterface = "ws.scrabble.ScrabbleWebService", targetNamespace = "http://scrabble.ws/", wsdlLocation = "WEB-INF/wsdl/gameWS/ScrabbleWebServiceService.wsdl")
public class gameWS {
    
    HashMap<String, Game> gameMap = new HashMap();
    HashMap<Integer, Game> playersToGameMap = new HashMap();
    int playersIdCounter = 0;

    public java.util.List<ws.scrabble.Event> getEvents(int playerId, int eventId) throws ws.scrabble.InvalidParameters_Exception {
        if(!playersToGameMap.containsKey(playerId)) {
            throw new ws.scrabble.InvalidParameters_Exception("Player doesn't belong to any game", null);
        }
        
        Game existingGame = playersToGameMap.get(playerId);
        List<Event> events = existingGame.getNewEvents(eventId);
        
        if (events == null) 
            throw new ws.scrabble.InvalidParameters_Exception("eventID is invalid", null);
        return events;
    }

    public java.lang.String createGameFromXML(java.lang.String xmlData) throws ws.scrabble.InvalidParameters_Exception, ws.scrabble.InvalidXML_Exception, ws.scrabble.DuplicateGameName_Exception {
        Game newGame = new Game(); 
        
        try {
            newGame.loadGame(xmlData, playersIdCounter);
            playersIdCounter += 4;
            
            String gameName = newGame.getName();
            if(gameMap.containsKey(gameName)) {
                throw new DuplicateGameName_Exception("Game name: " + gameName + " already exists", null); 
            } 
            gameMap.put(gameName, newGame);
            
            for(Player player : newGame.getPlayers()) {
                    playersToGameMap.put(player.getID(), newGame); 
            }
                        
            return gameName;
        } catch(Game.GameException ex) {
            throw new InvalidXML_Exception(ex.text, null);
        } catch(PlayersManager.PlayerManagerException ex) {
            throw new InvalidParameters_Exception(ex.text, null);
        }
    }

    public java.util.List<ws.scrabble.PlayerDetails> getPlayersDetails(java.lang.String gameName) throws ws.scrabble.GameDoesNotExists_Exception, ws.scrabble.InvalidParameters_Exception {
        if(!gameMap.containsKey(gameName)) {
            throw new ws.scrabble.GameDoesNotExists_Exception("Game name: " + gameName + " doesn't exists", null);
        }
        
        Game existingGame = gameMap.get(gameName);
        List<PlayerDetails> playerDetails = new ArrayList();
        
        for(Player player : existingGame.getPlayers()) {
            playerDetails.add(getPlayerDetails(player.getID()));
        }
        
        return playerDetails;
    }

    public java.util.List<java.lang.String> getActiveGames() {
        List<String> activeGames = new ArrayList();
        
        for(Game game : gameMap.values()) {
            if(game.getStatus() == GameStatus.ACTIVE) {
                activeGames.add(game.getName());
            }
        }
        
        return activeGames;
    }

    public ws.scrabble.PlayerDetails getPlayerDetails(int playerId) throws ws.scrabble.InvalidParameters_Exception, ws.scrabble.GameDoesNotExists_Exception {
        if(!playersToGameMap.containsKey(playerId)) {
            throw new ws.scrabble.GameDoesNotExists_Exception("Player doesn't belong to any game", null);
        }
        try {
            Game existingGame = playersToGameMap.get(playerId);
            Player player = existingGame.getPlayerByID(playerId);

            PlayerDetails playerDetails = new PlayerDetails();
            playerDetails.setLetters(player.getCardsLetters());
            playerDetails.setName(player.getName());
            playerDetails.setScore(player.getScore());
            playerDetails.setStatus(player.getStatus());
            playerDetails.setType(player.getType());

            return  playerDetails;
        } catch(PlayersManager.PlayerManagerException ex) {
            throw new ws.scrabble.GameDoesNotExists_Exception(ex.text, null);
        }
    }

    public void passTurn(int playerId, int eventId) throws ws.scrabble.InvalidParameters_Exception {
        if(!playersToGameMap.containsKey(playerId)) {
            throw new ws.scrabble.InvalidParameters_Exception("Player doesn't belong to any game", null);
        }
        
        Game existingGame = playersToGameMap.get(playerId);
        if (existingGame.getLastEventID() != eventId)
            throw new ws.scrabble.InvalidParameters_Exception("Invalid event id", null);
        
        existingGame.passTrun();
    }

    public void createGame(java.lang.String name, int humanPlayers, int computerizedPlayers) throws ws.scrabble.DuplicateGameName_Exception, ws.scrabble.InvalidParameters_Exception {
        if(gameMap.containsKey(name)) {
            throw new ws.scrabble.DuplicateGameName_Exception("Game name: " + name + " already exists", null);
        }
        
        try {
            Game newGame = new Game(); 
            newGame.createNewGame(name, humanPlayers, computerizedPlayers, playersIdCounter);
            gameMap.put(name, newGame);
            for (Player player: newGame.getPlayers()) {
                if (!player.isHuman()) {
                    this.playersToGameMap.put(player.getID(), newGame);
                }
            }
            playersIdCounter += 4;
        } catch (Game.GameException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        } catch (Deck.DeckException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        } catch (PlayersManager.PlayerManagerException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        } catch (Player.PlayerException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        }
    }

    public java.util.List<java.lang.String> getWaitingGames() {
        List<String> waitingGames = new ArrayList();
        
        for(Game game : gameMap.values()) {
            if(game.getStatus() == GameStatus.WAITING) {
                waitingGames.add(game.getName());
            }
        }
        
        return waitingGames;       
    }

    public int joinGame(java.lang.String gameName, java.lang.String playerName) throws ws.scrabble.GameDoesNotExists_Exception, ws.scrabble.InvalidParameters_Exception {
        if(!gameMap.containsKey(gameName)) {
            throw new ws.scrabble.GameDoesNotExists_Exception("Game does not exists", null);
        }
        
        Game existingGame = gameMap.get(gameName);
        if(existingGame.getStatus() == GameStatus.ACTIVE) {
            throw new ws.scrabble.InvalidParameters_Exception("Can't join an active gsame", null);
        }
        
        try {
            int newID = existingGame.addHumanPlayer(playerName);
            if(!existingGame.isLoadedFromXML()) {
                this.playersToGameMap.put(newID, existingGame);
            }
            Player player = existingGame.getPlayerByName(playerName);
            if(player == null) {
                throw new InvalidParameters_Exception("Player name is not found in game!", null);
            }
            return player.getID();
        } catch(Game.GameException ex) {
            throw new ws.scrabble.GameDoesNotExists_Exception(ex.text, null);
        } catch (Player.PlayerException ex) {
            throw new ws.scrabble.GameDoesNotExists_Exception(ex.text, null);
        } catch (Deck.DeckException ex) {
            throw new ws.scrabble.GameDoesNotExists_Exception(ex.text, null);
        }
    }

    public ws.scrabble.GameDetails getGameDetails(java.lang.String gameName) throws ws.scrabble.GameDoesNotExists_Exception {
        if(!gameMap.containsKey(gameName)) {
            throw new ws.scrabble.GameDoesNotExists_Exception("Game does not exists", null);
        }
        
        Game existingGame = gameMap.get(gameName);
        
        GameDetails gameDetails = new GameDetails();
        gameDetails.setHumanPlayers(existingGame.getNumOfHumanPlayers());
        gameDetails.setComputerizedPlayers(existingGame.getNumOfComputerizedPlayers());
        gameDetails.setJoinedHumanPlayers(existingGame.getNumOfJoindHumanPlayers());
        gameDetails.setStatus(existingGame.getStatus());
        gameDetails.setLoadedFromXML(existingGame.isLoadedFromXML());
        gameDetails.setName(gameName);
        
        return gameDetails;
    }

    public java.lang.String replaceLetters(int playerId, int eventId, java.lang.String letters) throws ws.scrabble.InvalidParameters_Exception {
        if(!playersToGameMap.containsKey(playerId)) {
            throw new ws.scrabble.InvalidParameters_Exception("Player doesn't belong to any game", null);
        }
        
        Game existingGame = playersToGameMap.get(playerId);
        if (existingGame.getLastEventID() != eventId)
            throw new ws.scrabble.InvalidParameters_Exception("Invalid event id", null);
        
        
        ArrayList<String> lettersArr = new ArrayList<>();
        for (char c : letters.toCharArray()){
            lettersArr.add(String.valueOf(c));
        }
        try {
            return existingGame.exchangeCards(lettersArr);
        } catch (Player.PlayerException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        } catch (Deck.DeckException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        }
    }

    public void resign(int playerId) throws ws.scrabble.InvalidParameters_Exception {
        if(!playersToGameMap.containsKey(playerId)) {
            throw new ws.scrabble.InvalidParameters_Exception("Player doesn't belong to any game", null);
        }
        
        try {
            Game existingGame = playersToGameMap.get(playerId);
            existingGame.resign(playerId);
        } catch(PlayersManager.PlayerManagerException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        }
    }

    public void makeWord(int playerId, int eventId, java.lang.String lettersStartPosition, ws.scrabble.Orientation lettersOrientation, java.lang.String letters, java.lang.String wordStartPosition, ws.scrabble.Orientation wordOrientation, java.lang.String word) throws ws.scrabble.InvalidParameters_Exception {
        if(!playersToGameMap.containsKey(playerId)) {
            throw new ws.scrabble.InvalidParameters_Exception("Player doesn't belong to any game", null);
        }
        
        Game existingGame = playersToGameMap.get(playerId);
        if (existingGame.getLastEventID() != eventId)
            throw new ws.scrabble.InvalidParameters_Exception("Invalid event id", null);
        
        try {
            existingGame.validateWordAndSubmit(lettersStartPosition,
                    lettersOrientation,
                    letters,
                    wordStartPosition,
                    wordOrientation,
                    word);
        } catch (Cell.CellException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        } catch (Player.PlayerException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        } catch (Board.BoardException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        } catch (Game.GameException ex) {
            throw new ws.scrabble.InvalidParameters_Exception(ex.text, null);
        }
    }
}
