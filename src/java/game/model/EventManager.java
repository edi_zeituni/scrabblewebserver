/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import java.util.ArrayList;
import java.util.List;
import ws.scrabble.Event;
import ws.scrabble.EventType;
import ws.scrabble.GameStatus;
import ws.scrabble.Orientation;
import ws.scrabble.PlayerAction;

/**
 *
 * @author Edi
 */
public class EventManager {
    List<Event> eventList;
    private int eventIdsCounter;
    
    public EventManager(){
        eventList = new ArrayList();
        eventIdsCounter = 0;
    }
    
    public void gameStart(){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.GAME_START);
        eventList.add(event);
    }
    
    public void playerTurn(String playerName){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.PLAYER_TURN);
        event.setPlayerName(playerName);
        eventList.add(event);
    }
    
    public void promptPlayerToTakeAction(String playerName){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.PROMPT_PLAYER_TO_TAKE_ACTION);
        event.setPlayerName(playerName);
        event.setTimeout(120);
        eventList.add(event);
    }
    
    public void lettersOnBoard(String letters, String position, Orientation orientation){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.LETTERS_ON_BOARD);
        event.setLetters(letters);
        event.setPosition(position);
        event.setOrientation(orientation);
        eventList.add(event);
    }
    
    public void gameWinner(String winnerPlayerName){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.GAME_WINNER);
        event.setPlayerName(winnerPlayerName);
        eventList.add(event);
    }
    
    public void gameOver(){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.GAME_OVER);
        eventList.add(event);
    }
    
    public void playerActionPassTurn(String playerName){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.PLAYER_ACTION);
        event.setPlayerAction(PlayerAction.PASS_TURN);
        event.setPlayerName(playerName);
        eventList.add(event);
    }
    
    public void playerActionReplaceLetters(String playerName){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.PLAYER_ACTION);
        event.setPlayerAction(PlayerAction.REPLACE_LETTERS);
        event.setPlayerName(playerName);
        eventList.add(event);
    }
    
    public void playerActionMakeWord(String playerName, String letters, Orientation orientation, String lettersStartPos, int score, String word)
    {
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.PLAYER_ACTION);
        event.setPlayerAction(PlayerAction.MAKE_WORD);
        event.setPlayerName(playerName);
        event.setLetters(letters);
        event.setOrientation(orientation);
        event.setPosition(lettersStartPos);
        event.setScore(score);
        event.setWord(word);
        eventList.add(event);
    }
    
    public void playerResigned(String playerName){
        Event event = new Event();
        event.setId(eventIdsCounter++);
        event.setType(EventType.PLAYER_RESIGNED);
        event.setPlayerName(playerName);
        eventList.add(event);
    }
    
    public int getLastEventID(){
        return this.eventList.get(eventList.size()-1).getId();
    }
    
    public List<Event> getNewEvents(int lastEventID){
        int lastIndex = eventList.size()-1;
        if (lastEventID == 0)
            return eventList;
        else if (lastEventID < 0 || (eventList.get(lastIndex).getId() < lastEventID) )
            return null;
        else if (eventList.get(lastIndex).getId() == lastEventID)
            return new ArrayList(); 
        else{
            for (int i = lastIndex; i > 0; i--) {
                if (eventList.get(i).getId() == lastEventID) 
                    return eventList.subList(i+1, lastIndex+1);
            }
        }
        return null;
    }
}
