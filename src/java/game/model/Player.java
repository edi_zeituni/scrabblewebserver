/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import java.util.ArrayList;
import java.util.Objects;
import ws.scrabble.PlayerStatus;
import ws.scrabble.PlayerType;

/**
 *
 * @author danielo
 */
public class Player {
    PlayerStatus status;
    private int score;
    private ArrayList<Card> cardsList;
    int id;
    
    public static final int CARDS_FOR_PLAYER = 7;
    private final String NAME;
    private final PlayerType TYPE;

    
    public Player(String name, Boolean isHuman, int id){
        status = PlayerStatus.ACTIVE;
        cardsList = new ArrayList<>();
        this.NAME = name;
        this.TYPE = isHuman ? PlayerType.HUMAN : PlayerType.COMPUTER;
        this.id = id;
        score = 0;
    }
    
    public String getName() {
        return this.NAME;
    }
    
    public int getScore() {
        return this.score;
    }
    
    public PlayerType getType() {
        return this.TYPE;
    }
    
    public int getNumOfCard() {
        return cardsList.size();
    }
    
    public int getID() {
        return id;
    }
    
    public PlayerStatus getStatus() {
        return status;
    }
    
    public void reset() {
        cardsList = new ArrayList<>();
        score = 0;
    }
    
    public String getCardsLetters(){
        StringBuffer res = new StringBuffer("");
        for(Card c:cardsList){
            res.append(c.getLetter() + " ");
        }
        return res.toString();
    }
    
    public ArrayList<String> getCardsLettersAsArray(){
        ArrayList<String> res = new ArrayList<>();
        for(Card c: cardsList){
            res.add(String.valueOf(c.getLetter()));
        }
        return res;
    }
    
    public ArrayList<Card> getCards()
    {
        return cardsList;
    }
    public void updateScore(int points)
    {
        this.score += points;
    }
    
    public void addCard(Card card, int indx) throws PlayerException{
        if (cardsList.size()>=CARDS_FOR_PLAYER) 
            throw new PlayerException(PlayerException.ErrorType.PLAYER_FULL_OF_CARDS, null);
        cardsList.add(indx, card);
    }
    
    public void addCard(Card card) throws PlayerException{
        if (cardsList.size()>=CARDS_FOR_PLAYER) 
            throw new PlayerException(PlayerException.ErrorType.PLAYER_FULL_OF_CARDS, null);
        cardsList.add(card);
    }
    
    public Card getCardAndRemove(int indx) throws PlayerException{
        if(cardsList.isEmpty())
            throw new PlayerException(PlayerException.ErrorType.PLAYER_HAS_NO_CARDS, null);
        
        Card card = cardsList.get(indx);
        removeCard(card);
        return card;
    }
    
    public Card getCard(int indx) throws PlayerException{
        if(cardsList.isEmpty())
            throw new PlayerException(PlayerException.ErrorType.PLAYER_HAS_NO_CARDS, null);
        
        Card card = cardsList.get(indx);
        return card;
    }
    
    public void removeCard(Card card){
        cardsList.remove(card);
    }
    
    public Boolean isHoldLetter(char ch){
        Card joker = null;
        for (Card c : cardsList) {
            if (c.getLetter() == ch) {
                return true;
            }
            if (c.getLetter() == ' ') {
                joker = c;
            }
        }
        if (joker != null) {
            joker.setJokerLetter(ch);
            return true;
        }
        else
            return false;
    }
    
    public int getCardLetterValue(char ch){
        for (Card c : cardsList) {
            if (c.getLetter() == ch) {
                return c.getValue();
            }
        }
        return 0;
    }
    
    //return the index of the card in cardsList
    public int removeCardWithLetter(char ch){
        Card joker = null;
        int jokerIndx = -1;
        
        int i = 0;
        for (Card c : cardsList) {
            if (c.getLetter() == ch) {
                removeCard(c);
                return i;
            }
            if (c.getLetter() == ' '){
                joker = c;
                jokerIndx = i;
            }
            i++;
        }
        if (joker != null)
            removeCard(joker);
        return jokerIndx;
    }
    
    public void setScore(int score) {
        this.score = score;    
    }
    
    public void setStatus(PlayerStatus status) {
        this.status = status;
    }
    
    @Override
    public int hashCode() {
	int hash = 7;
	hash = 47 * hash + Objects.hashCode(this.NAME);
	return hash;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final Player other = (Player) obj;
	if (!Objects.equals(this.NAME, other.NAME)) {
	    return false;
	}
	return true;
    }
    
    public boolean isHuman() {
        return this.TYPE == PlayerType.HUMAN;
    }
    
    public static class PlayerException extends Exception {
        public enum ErrorType { PLAYER_FULL_OF_CARDS,
                                PLAYER_HAS_NO_CARDS,
                                PLAYER_DOESNT_HOLD_CARD};
        public String text;
        public String type;
        public int code;

        public PlayerException(ErrorType type, String additionalData) {
            this.type = type.toString();
            switch (type){
                case PLAYER_FULL_OF_CARDS:
                    code = 501;
                    text = "Player already have cards";
                    break;
                case PLAYER_HAS_NO_CARDS:
                    code = 502;
                    text = "Player doesn't hold cards";
                    break;
                case PLAYER_DOESNT_HOLD_CARD:
                    code = 503;
                    text = "Player doesn't hold card: " + additionalData;
                    break;
                default:
                    code = 500;
                    text = "General error: " + additionalData;
                    break;
            }
        }
    }
}
