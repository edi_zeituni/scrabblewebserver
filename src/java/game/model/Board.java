/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import java.util.List;
import java.util.Random;
import ws.scrabble.Orientation;


/**
 *
 * @author danielo
 */
public class Board {
    // bonus cell initilazation constants
    private static final int NUM_OF_BONUS_CELL_TYPE = 4;
    private static final int[] DOUBLE_LETTER_X = new int[] {0,0,2,2,3,3,3,6,6,6,6,7,7,8,8,8,8,11,11,11,12,12,14,14};
    private static final int[] DOUBLE_LETTER_Y = new int[] {3,11,6,8,0,7,14,2,6,8,12,3,11,2,6,8,12,0,7,14,6,8,3,11};
    private static final int[] TRIPLE_LETTER_X = new int[] {1,1,5,5,5,5,9,9,9,9,13,13};
    private static final int[] TRIPLE_LETTER_Y = new int[] {5,9,1,5,9,13,1,5,9,13,5,9};
    private static final int[] DOUBLE_WORD_X = new int[] {1,1,2,2,3,3,4,4,7,10,10,11,11,12,12,13,13};
    private static final int[] DOUBLE_WORD_Y = new int[] {1,13,2,12,3,11,4,10,7,4,10,3,11,2,12,1,13};
    private static final int[] TRIPLE_WORD_X = new int[] {0,0,0,7,7,14,14,14};
    private static final int[] TRIPLE_WORD_Y = new int[] {0,7,14,0,14,0,7,14};
    
    // board dimension constants
    public static final int COL = 15;
    public static final int ROW = 15;
    
    Cell[][] board; 
    Boolean isBoardEmpty;
    
    public Board() {
        board = new Cell[COL][ROW];
        isBoardEmpty = true;
        
        // creating board bonus cells
        for(int i=0; i < NUM_OF_BONUS_CELL_TYPE; i++) {
            for(int j=0; j < numOfBonusCell(i); j++) {
                 createBonusCell(i,j);
            }
        }
        
        // creating board default cells (no bonus)
        for(int i=0; i < COL; i++) {
            for(int j=0; j < ROW; j++) {
                if (board[i][j] == null) {
                    board[i][j] = new Cell(i, j, Cell.CellBonus.Nobonus);
                }
            }
        }
    }
    
    public void setBoardWithGeneratedCellList(List<generated.Cell> cellList) throws BoardException{
        generated.Cell gCell;
        int row;
        int col;
        Card card;
        char cLetter;
        Boolean isSeenH8 = false;
        for (int i = 0; i < cellList.size(); i++) {
            gCell = cellList.get(i);
            String pos = "" + gCell.getCol() + gCell.getRow();
            if (!isPositionInBounds(pos))
                throw new BoardException(BoardException.ErrorType.OUT_OF_BOUNDS, pos);
            if (pos.equals("H8")) {
                isSeenH8 = true;
            }
            String letter = gCell.getLetter();
            letter = letter.toUpperCase();
            cLetter = letter.charAt(0);
            if (letter.length()!=1 || cLetter < 'A' || cLetter > 'Z') {
                throw new BoardException(BoardException.ErrorType.UNSUPPORTED_LETTER, letter);
            }
            row = getRowNumFromPosition(pos);
            col = getColNumFromPosition(pos);
            if (!this.board[col][row].isEmpty()) {
                throw new BoardException(BoardException.ErrorType.CELL_NOT_EMPTY, pos);
            }
            card = new Card(cLetter, 0);
            this.board[col][row].setCard(card);
        }
        
        if (!isSeenH8) {
            throw new BoardException(BoardException.ErrorType.H8_MISSING, null);
        }
        if(cellList.size() > 0) {
            this.isBoardEmpty = false;
        } else {
            this.isBoardEmpty = true;
        }
    }
    
    private int numOfBonusCell(int i) {
        
        switch (i) {
            case 0:
                return DOUBLE_LETTER_X.length;
            case 1:
                return TRIPLE_LETTER_X.length;
            case 2:
                return DOUBLE_WORD_X.length;
            case 3: 
                return TRIPLE_WORD_X.length;
        }
        
        System.err.println("numOfBonusSize Error");
        System.exit(1);
        return -1;
    }
    
    private void createBonusCell(int i, int j){
        int x, y;
        
        switch (i) {
            case 0:
                x = DOUBLE_LETTER_X[j];
                y = DOUBLE_LETTER_Y[j];
                board[x][y] = new Cell(x, y, Cell.CellBonus.DoubleLetter);
                break;
            case 1:
                x = TRIPLE_LETTER_X[j];
                y = TRIPLE_LETTER_Y[j];
                board[x][y] = new Cell(x, y, Cell.CellBonus.TripleLetter);
                break;
            case 2:
                x = DOUBLE_WORD_X[j];
                y = DOUBLE_WORD_Y[j];
                board[x][y] = new Cell(x, y, Cell.CellBonus.DoubleWord);
                break;
            case 3:
                x = TRIPLE_WORD_X[j];
                y = TRIPLE_WORD_Y[j];
                board[x][y] = new Cell(x, y, Cell.CellBonus.TripleWord);
                break;
        }
    }
    
    public Cell getCell(int x, int y) {
        return board[x][y];
    }
    
    //A validation function that get cells to be changed.
    //function doesn't change the board, just checks.
    //this.isBoardEmpty should be changed on first submit.
    //Returns a boolean, true on success / false on failure.
    public Boolean validateNewWordOnBoard(String lettersStartPos, 
                                          Orientation lettersOrientation,
                                          String letters,
                                          String wordStartPos, 
                                          Orientation wordOrientation,
                                          String word) throws Cell.CellException, BoardException
    {
        if (this.isBoardEmpty && !lettersStartPos.equals("H8")){
            throw new BoardException(BoardException.ErrorType.H8_MISSING, lettersStartPos);
        }
        if (!isWordInBounds(lettersStartPos, lettersOrientation, letters)) {
            throw new BoardException(BoardException.ErrorType.OUT_OF_BOUNDS, null);
        }
        if (!isWordInBounds(wordStartPos, wordOrientation, word)) {
            throw new BoardException(BoardException.ErrorType.OUT_OF_BOUNDS, null);
        }
        if (!isCellsAreClear(lettersStartPos, lettersOrientation, letters)) {
            throw new BoardException(BoardException.ErrorType.CELL_NOT_EMPTY, null);
        }
        if (!isCellBeforeAndAfterClear(wordStartPos, wordOrientation, word)) {
            throw new BoardException(BoardException.ErrorType.PREVIOUS_CELL_NOT_EMPTY, null);
        }
        if (!this.isBoardEmpty && !isWordAndNewLettersAreConnected(lettersStartPos,
                                             lettersOrientation,
                                             letters,
                                             wordStartPos, 
                                             wordOrientation,
                                             word)) {
            throw new BoardException(BoardException.ErrorType.UNCONNECTED_LETTERS, null);
        }
        
        int wordCol = getColNumFromPosition(wordStartPos);
        int wordRow = getRowNumFromPosition(wordStartPos);
        int letCol = getColNumFromPosition(lettersStartPos);
        int letRow = getRowNumFromPosition(lettersStartPos);
        int addToCol = 0;
        int addToRow = 0;
        
        if (wordOrientation == Orientation.HORIZONTAL)
            addToCol =1;
        else
            addToRow = 1;
        
        Card card;
        Boolean isAtLeastOneLetterOnBoard = false;
        Boolean isAtLeastOneNewLetter = false;
        int i;
        int j;
        for (i = j = 0; i < word.length(); ) 
        {
            while(wordCol<letCol || wordRow<letRow || (isAtLeastOneNewLetter && i < word.length()))
            {
                card = this.board[wordCol][wordRow].getCard();
                if (word.charAt(i)!= card.getLetter()) {
                    throw new BoardException(BoardException.ErrorType.VALIDATION_EXISTING_FAIL, word);
                }
                isAtLeastOneLetterOnBoard = true;
                i++;
                wordCol += addToCol;
                wordRow += addToRow;
            }
            for (; j < letters.length(); j++,i++) {
                if (letters.charAt(j) != word.charAt(i)) {
                    throw new BoardException(BoardException.ErrorType.VALIDATION_LETTERS_FAIL,
                                                    "word: " + word + ", letters: " + letters);
                }
                isAtLeastOneNewLetter = true;
                wordCol += addToCol;
                wordRow += addToRow;
            }
        }
        
        return ((isAtLeastOneLetterOnBoard && isAtLeastOneNewLetter) || isBoardEmpty);
    }
    
    //position format "H8", "A3" etc.
    private Boolean isPositionInBounds(String position){
        int posLentght = position.length();
        if (!(posLentght == 2 || posLentght ==3)) {
            return false;
        }
        int col = getColNumFromPosition(position);
        int row = getRowNumFromPosition(position);
        return ((col>=0) && (col<COL) && (row>=0) && (row<ROW));
    }
    
    private int getColNumFromPosition(String position){
        return position.charAt(0) - 'A';
    }
    
    private int getRowNumFromPosition(String position){
        int row = position.charAt(1) - '1';
        if (position.length() == 3 ){
            row = position.charAt(1) - '0';
            row *= 10;
            row += position.charAt(2) - '1';
        }
        return row;
    }
    
    private Boolean isWordInBounds(String startPos, 
                                   Orientation orientation,
                                   String word)
    {
        if (!isPositionInBounds(startPos))
           return false;
        String endPosition;
        if (orientation == Orientation.HORIZONTAL) {
            char col = startPos.charAt(0);
            for (int i = 0; i < word.length()-1 ; i++) {
                col += 1;
            }
            endPosition = "" + col + startPos.charAt(1);
            if (startPos.length() == 3) {
               endPosition = endPosition + startPos.charAt(2);
            }
        }
        else
        {
            int row = getRowNumFromPosition(startPos);
            row = row + word.length();
            endPosition = "" + startPos.charAt(0) + row;
        }
        return isPositionInBounds(endPosition);
    }
    
    private Boolean isCellsAreClear(String startPos, 
                                   Orientation orientation,
                                   String word)
    {
        int col = getColNumFromPosition(startPos);
        int row = getRowNumFromPosition(startPos);
        int addToCol = 0;
        int addToRow = 0;
        
        if (orientation == Orientation.HORIZONTAL)
            addToCol =1;
        else
            addToRow = 1;
        
        for (int i = 0; i < word.length(); i++) 
        {
            if (!this.board[col][row].isEmpty()) {
                return false;
            }
            col += addToCol;
            row += addToRow;
        }
        
        return true;
    }
    
    private Boolean isCellBeforeAndAfterClear(String startPos, Orientation orientation, String word){
        int col = getColNumFromPosition(startPos);
        int row = getRowNumFromPosition(startPos);
        
        if (orientation == Orientation.HORIZONTAL){
            if (col != 0 && !board[col-1][row].isEmpty())
                return false;
            if (col+word.length() == COL)
                return true;
            if (!board[col+word.length()][row].isEmpty())
                return false;
        }
        else{
            if (row != 0 && !board[col][row-1].isEmpty())
                return false;
            if (row+word.length() == ROW)
                return true;
            if (!board[col][row+word.length()].isEmpty())
                return false;
        }
        return true;
    }
    
        private Boolean isWordAndNewLettersAreConnected(String lettersStartPos, 
                                          Orientation lettersOrientation,
                                          String letters,
                                          String wordStartPos, 
                                          Orientation wordOrientation,
                                          String word)
    {
        int wordCol = getColNumFromPosition(wordStartPos);
        int wordRow = getRowNumFromPosition(wordStartPos);
        int letCol = getColNumFromPosition(lettersStartPos);
        int letRow = getRowNumFromPosition(lettersStartPos);
        if (wordOrientation == Orientation.HORIZONTAL) {
            if (wordRow!=letRow)
                return false;
            if (wordCol<letCol){
                int i = 0;
                int lettersCounter = letters.length();
                for ( ; i < word.length() ; i++) {
                    if (wordCol >= letCol && lettersCounter!=0) {
                        lettersCounter--;
                        letCol++;
                    }
                    else if (board[wordCol][wordRow].isEmpty())
                        return false;
                    wordCol++;
                }
                return lettersCounter==0;
            }
        }
        else{
            if (wordCol!=letCol)
                return false;
            if (wordRow<letRow){
                int i = 0;
                int lettersCounter = letters.length();
                for ( ; i < word.length() ; i++) {
                    if (wordRow >= letRow && lettersCounter!=0) {
                        lettersCounter--;
                        letRow++;
                    }
                    else if (board[wordCol][wordRow].isEmpty())
                        return false;
                    wordRow++;
                }
                return lettersCounter==0;
            }
        } 
        return true;
    }
    
    //returns an array with same lenght as letters, each int holds the correspond bonus value of the cell 
    public Cell.CellBonus[] changeCells(String lettersStartPos, 
                                          Orientation lettersOrientation,
                                          String letters)
    {
        Cell.CellBonus[] bonus = new Cell.CellBonus[letters.length()];
        int col = getColNumFromPosition(lettersStartPos);
        int row = getRowNumFromPosition(lettersStartPos);
        int addToCol = 0;
        int addToRow = 0;
        
        if (lettersOrientation == Orientation.HORIZONTAL)
            addToCol =1;
        else
            addToRow = 1;
        
        for (int i = 0; i < letters.length(); i++) 
        {
            Card c = new Card(letters.charAt(i),0);   
            this.board[col][row].setCard(c);
            bonus[i] = this.board[col][row].getCellBonus();
            col += addToCol;
            row += addToRow;
        }
        this.isBoardEmpty = false;
        return bonus;
    }
    
    public String getEmptyCellForComputer(){
        if (isBoardEmpty) {
            return "7,7";
        }
        
        int row = 7;
        int col = 7;
        Random randIndex = new Random();
        int direction = randIndex.nextInt(4);
        switch (direction){
            case 0: // seek right
            case 1: // seek left
                col = findEmptyCell(direction);
                break;
            case 2: // seek down
            default: // seek up
                row = findEmptyCell(direction);
                break;
        }
        if (row == -1 || col == -1) {
            return "";
        }
        return new String(String.valueOf(col) + "," + String.valueOf(row));
    }
    
    private int findEmptyCell(int direction){
        int row = 7;
        int col = 7;
        int addToCol = 0;
        int addToRow = 0;
        Cell tmp = null;
        switch (direction){
            case 0:
                addToCol = 1;
                break;
            case 1:
                addToCol = -1;
                break;
            case 2:
                addToRow = 1;
                break;
            default:
                addToRow = -1;
                break;
        }
        
        for (int i = 0 ; i < 7; i++) {
             if (board[col][row].isEmpty()){
                 tmp = board[col][row];
                 break;
             }
             col+=addToCol;
             row+=addToRow;
        }
        if (tmp == null)
            return -1;
        if (addToCol != 0)
            return col;
        else
            return row;
    }
    
    public static String getBoardLocationFormat(int x, int y) {
        String location;
        
        location = String.valueOf((char)(x + 'A'));
        location += String.valueOf(y + 1);
        
        return location;
    }
    
    public static String getBoardLocationFormat(Cell cell) {
        String location;
                
        location = String.valueOf((char)(cell.getX() + 'A'));
        location += String.valueOf(cell.getY() + 1);
        
        return location;
    }
    
    public static class BoardException extends Exception {
        public enum ErrorType { OUT_OF_BOUNDS,
                                UNSUPPORTED_LETTER,
                                CELL_NOT_EMPTY,
                                H8_MISSING,
                                UNCONNECTED_LETTERS,
                                VALIDATION_EXISTING_FAIL,
                                VALIDATION_LETTERS_FAIL,
                                PREVIOUS_CELL_NOT_EMPTY};
        public String text;
        public String type;
        public int code;
        
        public BoardException(ErrorType type, String additionalData) {
            this.type = type.toString();
            switch (type){
                case OUT_OF_BOUNDS:
                    code = 201;
                    text = "The position " + additionalData + " is out of bounds";
                    break;
                case UNSUPPORTED_LETTER:
                    code = 202;
                    text = "The letter " + additionalData + " is not supported";
                    break;
                case CELL_NOT_EMPTY:
                    code = 203;
                    text = "Cell " + " is not empty";
                    break;
                case H8_MISSING:
                    code = 204;
                    text = "First word must start from the center cell";
                    break;
                case UNCONNECTED_LETTERS:
                    code = 205;
                    text = "The cards are not connected";
                    break;
                case VALIDATION_EXISTING_FAIL:
                    code = 206;
                    text = "Existing letters of the word " + additionalData + " not found on board";
                    break;
                case VALIDATION_LETTERS_FAIL:
                    code = 207;
                    text = "Word and letters are not suitable. " + additionalData;
                    break;
                case PREVIOUS_CELL_NOT_EMPTY:
                    code = 208;
                    text = "The cells before or after the word is not clear";
                    break;
                default:
                    code = 200;
                    text = "General error: " + additionalData;
                    break;
            }
        }
    }
}