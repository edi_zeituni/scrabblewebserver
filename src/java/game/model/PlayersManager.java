/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Edi
 */
public class PlayersManager {

    private final Players playersModel;
    private Player currentPlayer;
    private int nextFreeID;
    
    
    public PlayersManager(int startPlayersIds) {
	playersModel = new Players();
        nextFreeID = startPlayersIds;
    }

    public Player addPlayer(String name, boolean isHuman) throws PlayerManagerException{
	if (name == null || name.isEmpty())
	    throw new PlayerManagerException(PlayerManagerException.ErrorType.PLAYER_INPUT_IS_EMPTY, null);
        Player newPlayer = new Player(name, isHuman, nextFreeID);
	if (playersModel.isPlayerExists(newPlayer)) 
	    throw new PlayerManagerException(PlayerManagerException.ErrorType.PLAYER_NAME_ALREADY_EXISTS, name);
        if (playersModel.size() > 3 ) 
            throw new PlayerManagerException(PlayerManagerException.ErrorType.MAXIMUM_PLAYERS_4, String.valueOf(4));    
        playersModel.addPlayer(newPlayer);
	
        nextFreeID++;
	return newPlayer;
    }

    public ArrayList<Player> getPlayers(){
	ArrayList<Player> sortedPlayersList = new ArrayList<>(playersModel.getPlayers());
	Collections.sort(sortedPlayersList, new PlayerComparator()) ;
	return sortedPlayersList;
    }
    
    public void removeAllPlayers(){
        playersModel.removeAll();
    }
    
    public int getNumOfPlayers() {
        return playersModel.size();
    }

    public void setCurrentPlayer(Player player) {
        this.currentPlayer = player;
    }
    
    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    
    public Boolean isCurrentPlayerSet(){
        return currentPlayer != null;
    }
    
    public Player getPlayerByName(String name) {
        for(Player player : playersModel.getPlayers()) {
            if(name.equals(player.getName())){
                return player;
            }
        }
        return null;
    }
    
    public Player getPlayerByID(int id) throws PlayerManagerException {
        for(Player player : playersModel.getPlayers()) {
            if(player.getID() == id) {
                return player;
            }
        }
        
        throw new PlayerManagerException(PlayerManagerException.ErrorType.PLAYER_ID_NOT_FOUND, String.valueOf(id));
    }
    
    public void resetPlayers(){
        for(Player player : getPlayers()) {
            player.reset();
        }
    }

    static class PlayerComparator implements Comparator<Player> {
	@Override
	public int compare(Player o1, Player o2) {
	    return o1.getName().compareTo(o2.getName());
	}
    }
    
    public String calcWinner() {
        int maxPoints = 0;
        Player maxPointsPlayer = null;
        String winnerPlayerName = null;
        
        
        for(Player p : getPlayers()) {
            if(maxPointsPlayer == null) {
                maxPointsPlayer = p;
                maxPoints = p.getScore();
                winnerPlayerName = p.getName();
                continue;
            }
            if(maxPoints == p.getScore()){
                winnerPlayerName = "Game Ends With A Tie";
                continue;
            }
            if(maxPoints < p.getScore()){
                maxPointsPlayer = p;
                maxPoints = p.getScore();
                winnerPlayerName = p.getName();
            }
        }
        return winnerPlayerName;
    }   
    
    public static class PlayerManagerException extends Exception {
        public enum ErrorType { PLAYER_INPUT_IS_EMPTY,
                                PLAYER_NAME_ALREADY_EXISTS,
                                MAXIMUM_PLAYERS_4,
                                PLAYER_ID_NOT_FOUND };
        public String text;
        public String type;
        public int code;

        public PlayerManagerException(ErrorType type, String additionalData) {
            this.type = type.toString();
            switch (type){
                case PLAYER_INPUT_IS_EMPTY:
                    code = 601;
                    text = "Name is empty";
                    break;
                case PLAYER_NAME_ALREADY_EXISTS:
                    code = 602;
                    text = "Name " + additionalData + " exists";
                    break;
                case MAXIMUM_PLAYERS_4:
                    code = 603;
                    text = "Maximum players: " + additionalData;
                    break;
                case PLAYER_ID_NOT_FOUND:
                    code = 604;
                    text = "Player id " + additionalData + " not found";
                    break;
                default:
                    code = 600;
                    text = "General error: " + additionalData;
                    break;
            }
        }
    }
    
}

