/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

/**
 *
 * @author Edi
 */
public class Cell {
    public enum CellBonus {DoubleLetter, TripleLetter, DoubleWord, TripleWord, Nobonus}
    private final int x;
    private final int y;
    private final CellBonus bonus;
    Card card;
    
    
    public Cell(int x, int y, CellBonus bonus){
        this.x = x;
        this.y = y;
        this.bonus = bonus;
    }
    
    public void setCard(Card card){
        this.card = card;
    }
    
    public CellBonus getCellBonus(){
        return this.bonus;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
    
    public Boolean isEmpty(){return this.card == null;}
    
    public Card getCard() throws CellException{
        if (this.card == null) {
            String row = String.valueOf(this.x+1);
            String col = String.valueOf((this.y)+'A');
            
            throw new CellException(CellException.ErrorType.CELL_IS_EMPTY, "" + col + row );
        }
        else{
            return this.card;  
        }
    }
    
    public static int getBonusValueForLetter(Cell.CellBonus bonus)
    {
        int multipleValue = 1;
        switch(bonus)
        {
            case DoubleLetter:
                multipleValue = 2;
                break;
            case TripleLetter:
                multipleValue = 3;
                break;
            default:
                multipleValue = 1;
                break;
        }
        return multipleValue;
    }
    
    public static int getBonusValueForWord(Cell.CellBonus bonus)
    {
        int multipleValue = 1;
        switch(bonus)
        {
            case DoubleWord:
                multipleValue = 2;
                break;
            case TripleWord:
                multipleValue = 3;
                break;
            default:
                multipleValue = 1;
                break;
        }
        return multipleValue;
    }
    
    public static class CellException extends Exception {
    public enum ErrorType { CELL_IS_EMPTY};
    public String text;
    public String type;
    public int code;
        
        public CellException(ErrorType type, String additionalData) {
            this.type = type.toString();
            switch (type){
                case CELL_IS_EMPTY:
                    code = 401;
                    text = "Cell is empty " + additionalData;
                    break;
                default:
                    code = 100;
                    text = "General error: " + additionalData;
                    break;
            }
        }
    }
}
