/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import game.model.Board.BoardException;
import game.model.Cell.CellException;
import game.model.Deck.DeckException;
import game.model.Player.PlayerException;
import java.io.File;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import mta.spellchecker.SpellChecker;
import mta.spellchecker.SpellCheckerException;
import ws.scrabble.Event;
import ws.scrabble.EventType;
import ws.scrabble.GameStatus;
import ws.scrabble.Orientation;
import ws.scrabble.PlayerAction;
import ws.scrabble.PlayerStatus;
import ws.scrabble.PlayerType;



/**
 *
 * @author Edi
 */
public class Game {
    String name;
    EventManager eventManager;
    int numOfHumanPlayers;
    int numOfComputerizedPlayers;
    int numOfJoinedHumanPlayers;
    
    Boolean isLoadedFromXml = false;
    GameStatus status;
    
    SpellChecker spellChecker;
    private Board board;
    private Deck deck;
    private PlayersManager playerManager;
    
    private String winnerPlayerName;
    private Player lastRoundPlayer = null;
    private int startPlayersIdsFrom;
    
    private final int MAX_PLAYERS = 4;
    private final int MIN_PLAYERS = 2;
    private final int MIN_HUMAN_PLAYER = 1;
    
    private final Boolean HUMAN = true;
    private final Boolean COMPUTER = false;
    private final int FIRST_PLAYER = 0;
    
    Thread timoutThread;
    
    public Game(){
        spellChecker = SpellChecker.getInstace();
        eventManager = new EventManager();
    }
    
    public void createNewGame(String name, int humanPlayers, int computerizedPlayers, int startPlayersIds) throws Deck.DeckException, GameException, PlayersManager.PlayerManagerException, PlayerException {
        board = new Board();
        deck = new Deck(); 
        deck.shuffle();
        lastRoundPlayer = null;
        numOfJoinedHumanPlayers = 0;
        
        // Parameter Check
        if(humanPlayers < MIN_HUMAN_PLAYER 
           || computerizedPlayers + humanPlayers > MAX_PLAYERS 
           || computerizedPlayers + humanPlayers < MIN_PLAYERS
           || computerizedPlayers < 0) 
        {
            throw new GameException(GameException.ErrorType.PLAYER_NUMBER_ERROR);
        }        
        this.name = name;
        this.numOfHumanPlayers = humanPlayers;
        this.numOfComputerizedPlayers = computerizedPlayers;
        this.startPlayersIdsFrom = startPlayersIds;
        playerManager = new PlayersManager(startPlayersIds);
        
        for (int i = 0; i < computerizedPlayers; i++) {
            this.playerManager.addPlayer("CPU" + i, false);
        }
        
        for (Player player : playerManager.getPlayers()) {
            for (int j = 0; j < Player.CARDS_FOR_PLAYER; j++) 
                player.addCard(deck.deal(), j);
        }
        status = GameStatus.WAITING;
    }
        
    public int addHumanPlayer(String playerName) throws GameException, PlayerException, DeckException{
        if(numOfHumanPlayers == 0) {
            throw new GameException(GameException.ErrorType.PLAYER_NUMBER_ERROR);
        }
        
        try {
            Player player = null;
            if (!isLoadedFromXml) {
                player = playerManager.addPlayer(playerName, HUMAN);
                for (int j = 0; j < Player.CARDS_FOR_PLAYER; j++) {
                   player.addCard(deck.deal(), j);
                }
            } else {
                player = playerManager.getPlayerByName(playerName);
            }
            
            if(player == null) {
                throw new GameException(GameException.ErrorType.PLAYER_NAME_NOT_EXIST);
            }
            playerManager.setCurrentPlayer(player);
            
            if(player.getStatus() == PlayerStatus.ACTIVE) {
                numOfJoinedHumanPlayers++;
                player.setStatus(PlayerStatus.JOINED);
            } else {
                throw new GameException(GameException.ErrorType.PLAYER_NAME_NOT_EXIST);
            }
            
            if (numOfHumanPlayers == numOfJoinedHumanPlayers) {
                status = GameStatus.ACTIVE;
                eventManager.gameStart();
                eventManager.playerTurn(playerName);
                eventManager.promptPlayerToTakeAction(playerName);
                timoutThread = new Thread(()->playerTimoutThread());
                timoutThread.setDaemon(false);
                timoutThread.start();
            }
            return player.getID();       
        } catch(PlayersManager.PlayerManagerException ex) {
            throw new GameException(GameException.ErrorType.PLAYER_MANAGER_ADD_ERROR);
        }
    }
    
    public void resetGame() throws DeckException, PlayerException{  
        board = new Board();
        deck = new Deck();
        deck.shuffle();
        lastRoundPlayer = null;
        
        playerManager.resetPlayers();
        
        for (Player player : playerManager.getPlayers()) {
            for (int j = 0; j < Player.CARDS_FOR_PLAYER; j++) {
                player.addCard(deck.deal(), j);
            }
        }
    }
    
    public void loadGame(String xmlData, int startPlayersIds) throws GameException, PlayersManager.PlayerManagerException {
        isLoadedFromXml = true;
        numOfJoinedHumanPlayers = 0;
        this.startPlayersIdsFrom = startPlayersIds;
        playerManager = new PlayersManager(startPlayersIdsFrom);
        board = new Board();
        deck = new Deck(); 
        deck.shuffle();
        lastRoundPlayer = null;
        
        StringReader xmlReader = new StringReader(xmlData);  
        
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(generated.Scrabble.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            generated.Scrabble sc = (generated.Scrabble)jaxbUnmarshaller.unmarshal(xmlReader);
            
            LoadPlayers(sc.getPlayers());
            
            boolean humanPlayerSeen = false;
            
            for(Player p : playerManager.getPlayers()) {
                if(p.isHuman()){
                    humanPlayerSeen = true;
                    break;
                }
            }
            
            if(!humanPlayerSeen){
                throw new GameException(GameException.ErrorType.LOAD_GAME_ERROR);
            }
            
            try 
            {
                LoadBoard(sc.getBoard());
            } 
            catch (Board.BoardException | Deck.DeckException ex) {
                System.out.println(ex);
                throw new GameException(GameException.ErrorType.LOAD_GAME_ERROR);
            }
            
            String currentPlayerName = sc.getCurrentPlayer();
            
            for(Player player : playerManager.getPlayers()) {
                if(currentPlayerName.equals(player.getName())) {
                    playerManager.setCurrentPlayer(player);
                    break;
                }
            }
            
            if(!playerManager.isCurrentPlayerSet()){
                throw new GameException(GameException.ErrorType.LOAD_GAME_ERROR);
            }
            
            name = sc.getName();
        } catch (JAXBException ex) {
            throw new GameException(GameException.ErrorType.LOAD_GAME_ERROR);
        }
        
        status = GameStatus.WAITING;
    }
    
    private void LoadPlayers(generated.Players p) throws PlayersManager.PlayerManagerException {

        for (generated.Players.Player loadedPlayer : p.getPlayer()) {
            Player player = null;
            //Player Inforamation From JAXB
            String name = loadedPlayer.getName();
            generated.PlayerType type = loadedPlayer.getType();
            BigInteger score = loadedPlayer.getScore();
            if(type == generated.PlayerType.COMPUTER) {
                player = playerManager.addPlayer(name, COMPUTER);
                numOfComputerizedPlayers++;
            }
            if(type == generated.PlayerType.HUMAN) {
                player = playerManager.addPlayer(name, HUMAN);
                numOfHumanPlayers++;
            }
            
            player.setScore(score.intValue());
            String letters = loadedPlayer.getLetters();
            for(int j=0; j < letters.length(); j++) {
                try {
                    char ch = letters.toUpperCase().charAt(j);
                    Card card = new Card(ch);
                    deck.remvoCard(card);
                    player.addCard(card, j);
                } catch (Deck.DeckException ex) {
                    System.out.println(ex.text);
                    System.exit(1);
                } catch (Player.PlayerException ex) {
                    System.out.println(ex.text);
                    System.exit(1);
                }
            }
        }
    }   
    
    private void LoadBoard(generated.Board b) throws Board.BoardException, Deck.DeckException {
        board = new Board();
        board.setBoardWithGeneratedCellList(b.getCell());
        removeExistingCardsOnBoardFromDeck(b.getCell());
        for (generated.Cell cell : b.getCell()){
            eventManager.lettersOnBoard(cell.getLetter().toUpperCase(), cell.getCol() + String.valueOf(cell.getRow()), Orientation.VERTICAL);
        }
    }
    
    private void removeExistingCardsOnBoardFromDeck(List<generated.Cell> cellList) throws Deck.DeckException {
        generated.Cell gCell;
        for (int i = 0; i < cellList.size(); i++) {
            gCell = cellList.get(i);
            deck.remvoCard(new Card(gCell.getLetter().toUpperCase().charAt(0)));
        }
    }
    
    public Board getBoard(){
        return this.board;
    }
    
    private void setTurnForNextPlayer(){
        int counter = 0;
        int playerIndex, startSearchFromIndex;
        Player currentPlayer = playerManager.getCurrentPlayer();
        ArrayList<Player> players = playerManager.getPlayers();
        int playersSize = players.size();
        Boolean isNoHumanPlayersLeft = true;
        for(playerIndex=0; playerIndex < playersSize; playerIndex++) {
            if(players.get(playerIndex) == currentPlayer)
                break;
        }
        
        if (playerIndex == playersSize-1) 
            startSearchFromIndex = 0;
        else
            startSearchFromIndex = playerIndex + 1;
        
        counter = 0;
        for(int i=startSearchFromIndex ; counter < playersSize; counter++) {
            if(players.get(i).getStatus() == PlayerStatus.RETIRED)
            {
                i++;
                if (i == playersSize) 
                    i = 0;
            }
            else
            {
                playerManager.setCurrentPlayer(players.get(i));
                isNoHumanPlayersLeft = false;
                break;
            }
                
        }
        
        currentPlayer = playerManager.getCurrentPlayer();
        eventManager.playerTurn(currentPlayer.getName());
        
        if(lastRoundPlayer == currentPlayer || isNoHumanPlayersLeft){
            winnerPlayerName = playerManager.calcWinner();
            eventManager.gameWinner(winnerPlayerName);
            eventManager.gameOver();
            return;
        }
        
        if(deck.isEmpty() && lastRoundPlayer == null) {
            System.out.println("DECK IS EMPTY");
            lastRoundPlayer = playerManager.getCurrentPlayer();
        }
        
        if (currentPlayer.isHuman()) {
            eventManager.promptPlayerToTakeAction(currentPlayer.getName());
            timoutThread = new Thread(()->playerTimoutThread());
            timoutThread.setDaemon(false);
            timoutThread.start();
        }
        else
            computerPlay();
    }
    
    private void computerPlay(){
        passTrun();
    }
    
    public void passTrun(){
        eventManager.playerActionPassTurn(playerManager.getCurrentPlayer().getName());
        if(playerManager.getCurrentPlayer().getType() == PlayerType.HUMAN) {
            timoutThread.stop();
        } 
        
        setTurnForNextPlayer();
    }
    
    public String getWinnerPlayerName() {
        return winnerPlayerName;
    }
    
    public String exchangeCards(ArrayList<String> cardLettersToExchange) throws Player.PlayerException, Deck.DeckException {
        String newCardsLetters = "";
        Player player = playerManager.getCurrentPlayer();
        ArrayList<String> oldPlayerLetters = player.getCardsLettersAsArray();
        for(String letter: cardLettersToExchange) {
            if(oldPlayerLetters.contains(letter)){
                int index = oldPlayerLetters.indexOf(letter);
                oldPlayerLetters.set(index, "*");
                Card oldCard = player.getCardAndRemove(index);
                Card newCard = deck.exchange(oldCard);
                player.addCard(newCard, index);
                newCardsLetters += newCard.getLetter();
            }
            else
                throw new Player.PlayerException(Player.PlayerException.ErrorType.PLAYER_DOESNT_HOLD_CARD, letter);
        }
        eventManager.playerActionReplaceLetters(player.getName());
        timoutThread.stop();
        setTurnForNextPlayer();
        return newCardsLetters;
    }
 
    public Boolean validateWordAndSubmit(String lettersStartPos, 
                                        Orientation lettersOrientation,
                                        String letters,
                                        String wordStartPos, 
                                        Orientation wordOrientation,
                                        String word) throws CellException,
                                        PlayerException, 
                                        BoardException,
                                        GameException{
        Boolean spellStatus = false;
        Boolean validateStatus = false;
        int points = 0;
        Player p = playerManager.getCurrentPlayer();
        
        validateStatus = board.validateNewWordOnBoard(lettersStartPos, wordOrientation, letters, wordStartPos, wordOrientation, word);
        if (!validateStatus) {
            throw new GameException(GameException.ErrorType.WORD_VALIDATION_ERROR);
        }
        
        try{
            spellStatus = spellChecker.spellCheck(word);
        } catch (SpellCheckerException ex) {
            throw new GameException(GameException.ErrorType.INTERNET_CONNECTION_ERROR);
        }
   
        if(spellStatus) 
        {
            Cell.CellBonus[] cellsBonus = board.changeCells(lettersStartPos, wordOrientation, letters);
            for (int i = 0; i < letters.length(); i++) {
                points += Cell.getBonusValueForLetter(cellsBonus[i])*p.getCardLetterValue(letters.charAt(i));
                int index = p.removeCardWithLetter(letters.charAt(i));
                try {
                    if (index < 0) {
                        throw new GameException(GameException.ErrorType.WORD_VALIDATION_ERROR);
                    }
                    p.addCard(deck.deal(), index);
                } catch(DeckException ex) {
                    break;
                }
            }
            for (Cell.CellBonus cellsBonu : cellsBonus) {
                points *= Cell.getBonusValueForWord(cellsBonu);
            }
            if (letters.length() == 7) {
                points += 50;
            }
        }
        else
            points = -5;
        
        p.updateScore(points);
        eventManager.playerActionMakeWord(p.getName(), letters, lettersOrientation, lettersStartPos, points, word);
        
        if (spellStatus) {
            eventManager.lettersOnBoard(letters, lettersStartPos, lettersOrientation);
        }
        timoutThread.stop();
        setTurnForNextPlayer();
        return validateStatus && spellStatus;
    }
    

    public void computerChangeCard(Player p) throws PlayerException, DeckException{
        Random randIndex = new Random();
        int index = randIndex.nextInt(p.getNumOfCard());
        Card oldCard = p.getCardAndRemove(index);
        Card newCard = deck.exchange(oldCard);
        p.addCard(newCard, index);
    }
    
    public String getEmptyCellForComputer(){
        return board.getEmptyCellForComputer();
    }
    
    public String getName() {
        return name;
    }
    
    public GameStatus getStatus() {
        return status;
    }
    
    public int getNumOfJoindHumanPlayers() {
        return numOfJoinedHumanPlayers;
    }
    
    public int getNumOfComputerizedPlayers() {
        return numOfComputerizedPlayers;
    }
    
    public int getNumOfHumanPlayers() {
        return numOfHumanPlayers;
    }
    
    public Boolean isLoadedFromXML() {
        return isLoadedFromXml;
    }
    
    public static class GameException extends Exception {
        public enum ErrorType {LOAD_GAME_ERROR, SAVE_GAME_ERROR, PASS_TURN_ERROR, WORD_VALIDATION_ERROR, 
                               INTERNET_CONNECTION_ERROR, NO_HUMAN_PLAYER, DUPLICATE_GAME_NAME, 
                               PLAYER_NUMBER_ERROR, PLAYER_MANAGER_ADD_ERROR, PLAYER_NAME_NOT_EXIST, 
                               PLAYER_NOT_IN_ACTIVE_STATE};
        public String text;
        public String type;
        public int code;
        
        public GameException(ErrorType type) {
            this.type = type.toString();
            switch (type){
                case LOAD_GAME_ERROR:
                    code = 101;
                    text = "Error Loading Game From File";
                    break;
                case SAVE_GAME_ERROR:
                    code = 102;
                    text = "Error Saving Game To File";
                    break;
                case PASS_TURN_ERROR:
                    code = 103;
                    text = "Passing Turn Encountered An Error";
                    break;
                case WORD_VALIDATION_ERROR:
                    code = 104;
                    text = "Word Validation Error";
                    break; 
                case INTERNET_CONNECTION_ERROR:
                    code = 105;
                    text = "Check your internet connection";
                    break;
                case NO_HUMAN_PLAYER:
                    code = 106;
                    text = "Human player required";
                    break;
                case DUPLICATE_GAME_NAME:
                    code = 107;
                    text = "Duplicate game name found";
                    break;
                case PLAYER_NUMBER_ERROR:
                    code = 108;
                    text = "Player number error";
                    break;
                case PLAYER_MANAGER_ADD_ERROR:
                    code = 109;
                    text = "Player manger error on adding new player";
                    break;
                case PLAYER_NAME_NOT_EXIST:
                    code = 110;
                    text = "Player name does not exist";
                    break;
                case PLAYER_NOT_IN_ACTIVE_STATE:
                    code = 111;
                    text = "Player is not in active state";
                    break;                
            }
        }
    }
    
    public Player getPlayerByID(int id) throws PlayersManager.PlayerManagerException{
        return playerManager.getPlayerByID(id);
    }
    
    public Player getPlayerByName(String name) {
        return playerManager.getPlayerByName(name);
    }
    
    public ArrayList<Player> getPlayers() {
        return playerManager.getPlayers();
    }
    
    public int getLastEventID(){
        return eventManager.getLastEventID();
    }
    
    public List<Event> getNewEvents(int lastEventID){
        return eventManager.getNewEvents(lastEventID);
    }
    
    public void resign(int playerId) throws PlayersManager.PlayerManagerException{
        Player player = getPlayerByID(playerId);
        
        player.setStatus(PlayerStatus.RETIRED);
        eventManager.playerResigned(player.getName());
    }
    
    public void playerTimoutThread() {

        try {
            System.out.println("Timeout Thread Started");
            Thread.sleep(300000);
            System.out.println("Timeout Thread Woken up");
            resign(playerManager.getCurrentPlayer().getID());
            setTurnForNextPlayer();
        } catch(InterruptedException | PlayersManager.PlayerManagerException ex) {
            System.out.println("ERROR IN TIMEOUT THREAD");
        } 
    }
    
 }

